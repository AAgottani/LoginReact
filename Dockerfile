
FROM node:16.14.0

WORKDIR /app

COPY package.json ./

COPY npm.lock ./

RUN npm install 

COPY . .

EXPOSE 3000

CMD ["npm", "start"]

FROM node:13.12.0-alpine

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./
COPY package-lock.json ./
RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g --silent

COPY . ./

CMD ["npm", "start"]


